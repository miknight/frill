function isHanzi(character) {
    // console.log("Checking if " + character + " is Hanzi.")
    // Reference: http://www.unicode.org/charts/unihangridindex.html
    var codepoint = character.charCodeAt()
    if (codepoint >= 0x3400  && codepoint <= 0x4DB5)  return true // CJK Unified Ideographs Extension A
    if (codepoint >= 0x4E00  && codepoint <= 0x9FA5)  return true // CJK Unified Ideographs
    if (codepoint >= 0xF900  && codepoint <= 0xFA2D)  return true // CJK Compatibility Ideographs
    if (codepoint >= 0x20000 && codepoint <= 0x2A6D6) return true // CJK Unified Ideographs Extension B
    if (codepoint >= 0x2F800 && codepoint <= 0x2FA1D) return true // CJK Compatibility Ideographs Supplement
    return false
}

function hasHanzi(word) {
    for (var i=0; i<word.length; i++) {
        if (isHanzi(word[i])) {
            return true
        }
    }
    return false
}

function isIframe() {
    return (window != window.parent)
}

function tonify(numbered_string) {
    var tone_marks = [
    ['a', 'e', 'o', 'i', 'u', 'ü'],
    ['ā', 'ē', 'ō', 'ī', 'ū', 'ǖ'],
    ['á', 'é', 'ó', 'í', 'ú', 'ǘ'],
    ['ǎ', 'ě', 'ǒ', 'ǐ', 'ǔ', 'ǚ'],
    ['à', 'è', 'ò', 'ì', 'ù', 'ǜ'],
    ['a', 'e', 'o', 'i', 'u', 'ü']
    ]
    var vowel_map = { 'a': 0, 'e': 1, 'o': 2, 'i': 3, 'u': 4, 'ü': 5 }
    return numbered_string.replace(/([\w]*?)([aeioungr]+:?)(\d)/, function(match, initial, final, tone) {
        final = final.replace('u:', 'ü')
        var new_final = final
        for (var vowel in vowel_map) {
            if (vowel == 'i' && final == 'iu') continue
            new_final = new_final.replace(vowel, tone_marks[tone][vowel_map[vowel]])
            if (final != new_final) {
                break
            }
        }
        return initial + new_final
    })
}

function formatPronunciation(numbered_string) {
    return numbered_string.split(/\s+/).map(tonify).join(' ')
}

function isWhitespace(str) {
    return /^\s+$/.test(str)
}
