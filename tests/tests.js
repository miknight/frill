module("Hanzi detection");
test("Identify hanzi characters", function() {
    ok(isHanzi("我"))
});
test("Identify non-hanzi characters", function() {
    ok(!isHanzi("a"))
    ok(!isHanzi(" "))
    ok(!isHanzi("é"))
    ok(!isHanzi("∞"))
    ok(!isHanzi("💩"))
});
test("Hanzi-containing string detection", function() {
    ok(hasHanzi("我"))
    ok(hasHanzi("我爱你"))
    ok(!hasHanzi("Hello there"))
    ok(hasHanzi("I 爱 you"))
});

module("DOM decoration");
test("Decorate single hanzi character", function() {
    var container = $('<div>美</div>');
    decorate(container);
    equal($(".frill-character", container).length, 1);
});
test("Decorate multiple hanzi characters", function() {
    var container = $('<div>美中</div>');
    decorate(container);
    equal($(".frill-character", container).length, 2);
});
test("Decorate non-hanzi and hanzi characters", function() {
    var container = $('<div>a美</div>');
    decorate(container);
    equal($(".frill-character", container).length, 2);
});
test("Undecorate hanzi characters", function() {
    var container = $('<div frilled="true"><frilly class="frill"><frilly class="frill-character">美</frilly></frilly></div>');
    undecorate(container);
    equal($(".frill-character", container).length, 0);
});
test("Don't decorate whitespace", function() {
    var container = $('<div> 美 </div>');
    decorate(container);
    equal($(".frill-character", container).length, 1);
});

module("DOM priming");
test("Prime single hanzi character", function() {
    var container = $('<div>美</div>');
    prime(container);
    ok($(container).attr("frilled"));
});
test("Don't prime decorated character", function() {
    var container = $('<frilly class="frill-character">美</frilly>');
    prime(container);
    ok(!$(container).attr("frilled"));
});
test("Don't prime container that has primed children", function() {
    var container = $('<div><frilly frilled="true">美</frilly></div>');
    prime(container);
    ok(!$(container).attr("frilled"));
});
test("Unprime parents when priming a child", function() {
    var container = $('<div frilled="true"><div id="prime-me">美</div></div>');
    prime($("#prime-me", container));
    ok(!$(container).attr("frilled"));
});
test("Unprime single hanzi character", function() {
    var container = $('<div>中</div>');
    prime(container);
    ok($(container).attr("frilled"));
    unprime(container);
    ok(!$(container).attr("frilled"));
});
test("Do not prime elements with no hanzi", function() {
    var container = $('<div>a</div>');
    prime(container);
    ok(!$(container).attr("frilled"));
});
test("Normalise text nodes when re-priming", function() {
    var container = $('<div><div id="one">中我</div></div>');
    var div = $("#one", container);
    prime(div);
    mark($('.frill-character:first', container), '中');
    unmarkAll(div);
    unprime(div);
    // There's a risk that text nodes will not be normalised in the div, so re-priming will isolate characters.
    equal($("#one", container).contents().length, 1);
});
test("Don't prime textareas", function() {
    var container = $('<textarea>美</textarea>');
    prime(container);
    ok(!$(container).attr("frilled"));
});
test("Don't prime empty content", function() {
    var container = $('<div>中<div>中</div> </div>');
    prime(container);
    equal($(".frill", container).length, 1);
});

module("Greedy matching");
test("Greedy match one character", function() {
    var container = $('<div>中</div>');
    prime(container);
    var char = $('.frill-character:first', container);
    equal(greedyMatch(char), '中');
});
test("Greedy match multiple characters", function() {
    var container = $('<div>第二次世界大战</div>');
    prime(container);
    var char = $('.frill-character:first', container);
    equal(greedyMatch(char), '第二次世界大战');
});
test("Stop greedy match at stop word", function() {
    var container = $('<div>第二次。世界大战</div>');
    prime(container);
    var char = $('.frill-character:first', container);
    equal(greedyMatch(char), '第二次');
});
test("Greedy match everything even when a substring is marked", function() {
    var container = $('<div><div id="one">中我</div></div>');
    var div = $("#one", container);
    prime(div);
    mark($('.frill-character:last', container), '我');
    equal(greedyMatch($('.frill-character:first', container)), '中我');
});

module("Converting to tone marks");
test("Basic single word conversion", function() {
    equal(formatPronunciation('wo3'), 'wǒ');
});
test("Conversion for sixth vowel", function() {
    equal(formatPronunciation('nu:3'), 'nǚ');
});
test("Conversion of multi-vowel word", function() {
    equal(formatPronunciation('kai4'), 'kài');
    equal(formatPronunciation('dou1'), 'dōu');
    equal(formatPronunciation('zhuo1'), 'zhuō');
    equal(formatPronunciation('xiang3'), 'xiǎng');
    equal(formatPronunciation('guo2'), 'guó');
    equal(formatPronunciation('xiong1'), 'xiōng');
    equal(formatPronunciation('xiu1'), 'xiū');
    equal(formatPronunciation('dui4'), 'duì');
});
test("Conversion of multi-character words", function() {
    equal(formatPronunciation('ba4 ba5'), 'bà ba');
    equal(formatPronunciation('zhong1 guo2'), 'zhōng guó');
    equal(formatPronunciation('Mei3 li4 jian1 He2 zhong4 guo2'), 'Měi lì jiān Hé zhòng guó');
});
test("Finals with no initials", function() {
    equal(formatPronunciation('o1'), 'ō');
    equal(formatPronunciation('er4'), 'èr');
    equal(formatPronunciation('ang1'), 'āng');
});
